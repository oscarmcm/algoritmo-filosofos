class Filosofo
	attr_reader :numero, :nombre	

	def initialize(nombre, numero, mesa)
		@nombre = nombre
		@numero = numero
		@mesa = mesa
	end	

	def comer!
		@mesa.tomar_tenedor!(self, :derecha)
		@mesa.tomar_tenedor!(self, :izquierda)
		puts "#{@nombre} comiendo..."
		sleep(1)
		@mesa.devolver_tenedor!(self)
		sleep(1)
	end

	def pensar
		puts "#{@nombre} pensando..."
		sleep(1)
	end

end
