class Mesa

	#constructor
	def initialize
		@tenedor = Hash.new
		5.times do |numero|
			@tenedor[numero] = :libre
		end
		
		@mutex = Mutex.new
	end

	def tomar_tenedor!(filosofo, posicion)

		@mutex.synchronize do

			if (posicion == :derecha)
				tomar_tenedor(filosofo.numero, filosofo, :derecha)
				
			elsif(posicion == :izquierda)

				numero = a_izquierda(filosofo.numero)
				tomar_tenedor(numero, filosofo, :izquierda)
			end

		end #mutex

	end

	def devolver_tenedor!(filosofo)
		derecha = filosofo.numero
		izquierda = a_izquierda(derecha)

		@mutex.synchronize do
			@tenedor[derecha] = :libre
			@tenedor[izquierda] = :libre

			puts "#{filosofo.nombre} devuelve los tenedores #{derecha} y #{izquierda}"
		end		
	end
	
	private
	def a_izquierda(numero)
		(numero - 1) % 5
	end

	def tomar_tenedor(numero, filosofo, direccion)
		while (@tenedor[numero] == :ocupado)
			puts "#{filosofo.nombre} => tenedor a #{direccion} #{numero} esta ocupado!"
			@mutex.sleep(2)
		end
		
		@tenedor[numero] = :ocupado
		puts "#{filosofo.nombre} tomo tenedor a #{direccion} #{numero}"
	end

end




















