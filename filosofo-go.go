package main

import (
    "fmt"
    "sync"
    "time"
    "runtime/pprof"
    "os"
    "flag"
)

type Fork struct {
    sync.Mutex
}

type Table struct {
    philosophers chan Philosopher
    forks []*Fork
}

func NewTable(forks int) *Table {
    t := new(Table)
    t.philosophers = make(chan Philosopher, forks - 1)
    t.forks = make([]*Fork, forks)
    for i := 0; i < forks; i++ {
        t.forks[i] = new(Fork)
    }
    return t
}

func (t *Table) PushPhilosopher(p Philosopher) {
    p.table = t
    t.philosophers <- p
}

func (t *Table) PopPhilosopher() Philosopher {
    p := <-t.philosophers
    p.table = nil
    return p
}

func (t *Table) TenedorDerecho(philosopherIndex int) *Fork {
    f := t.forks[philosopherIndex]
    return f
}

func (t *Table) TenedorIzquierdo(philosopherIndex int) *Fork {
    f := t.forks[(philosopherIndex + 1) % len(t.forks)]
    return f
}

type Philosopher struct {
    name string
    index int
    table *Table
    fed chan int
}

func (p Philosopher) Think() {
    fmt.Printf("%s esta pensando...\n", p.name)
    time.Sleep(3e9)
    p.table.PushPhilosopher(p)
}

func (p Philosopher) Eat() {
    p.GetForks()
    fmt.Printf("%s esta comiendo...\n", p.name)
    time.Sleep(3e9)
    p.PutForks()
    p.table.PopPhilosopher()
    p.fed <- 1
}

func (p Philosopher) GetForks() {
    rightFork := p.table.TenedorDerecho(p.index)
    rightFork.Lock()

    leftFork := p.table.TenedorIzquierdo(p.index)
    leftFork.Lock()
}

func (p Philosopher) PutForks() {
    rightFork := p.table.TenedorDerecho(p.index)
    rightFork.Unlock()

    leftFork := p.table.TenedorIzquierdo(p.index)
    leftFork.Unlock()
}
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
func main() {
    flag.Parse()
    if *cpuprofile != "" {
        f, err := os.Create(*cpuprofile)
        if err != nil {
            fmt.Println("Error: ", err)
        }
        pprof.StartCPUProfile(f)
        defer pprof.StopCPUProfile()
    }

    table := NewTable(5)
    philosophers := []Philosopher{
        Philosopher{"Oscar Cortez", 0, table, make(chan int)},
        Philosopher{"alexander Blanchard", 1, table, make(chan int)},
        Philosopher{"Jonathan Guadamuz", 2, table, make(chan int)},
        Philosopher{"Ruben Norori", 3, table, make(chan int)},
        Philosopher{"Cesar Marin", 4, table, make(chan int)},
    }

    for {
        for _, p := range philosophers {
            go func(p Philosopher){
                p.Think()
                p.Eat()
            }(p)
        }

        for _, p := range philosophers {
            <-p.fed
            fmt.Printf("%s se alimento.\n", p.name)
        }
    }

}
