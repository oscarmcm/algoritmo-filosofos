package filosofos_2;

class Semaphore extends Object {

    private int count;

    public Semaphore(int startingCount) {
        count = startingCount;
    }

    public void down() {
        synchronized (this) {
            while (count <= 0) {
                // We must wait
                try {
                    wait();
                } catch (InterruptedException ex) {
                }
            }
            // Decrementar el contador
            count--;
        }
    }

    public void up() {
        synchronized (this) {
            count++; //Aumenta el contador
            //utilizar el método notify para despertar al hilo central.
            if (count == 1) {
                notify();
            }
        }
    }
}
