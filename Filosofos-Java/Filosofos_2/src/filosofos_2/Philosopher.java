package filosofos_2;

import filosofos_2.Semaphore.*;

/**
 *
 * @author Alexander
 */
public class Philosopher extends Thread {

    public final static int N = 5;              // Número de Filósofos 

    public final static int THINKING = 0;       // Filósofos que estan pensando
    public final static int HUNGRY = 1;         // Filósofos que tienen hambre
    public final static int EATING = 2;         // Filósofos que estan comiendo

    private static int state[] = new int[N];    // Matriz que permite el seguimiento del estado de todo

    private static Semaphore mutex = new Semaphore(1);  // Exclusión mutua para regiones criticas.
    private static Semaphore s[] = new Semaphore[N];    // Una iteración por cada uno de los filósofos

    // Variables de instancias
    public int myNumber;                    // Número del filósofo.
    public int myLeft;                      // Número del palillo izquierdo
    public int myRight;                     // Número del palillo derecho

    public Philosopher(int i) {             // Establecer un filósofo
        myNumber = i;
        myLeft = (i + N - 1) % N;               // cálculo para el palillo izquierdo
        myRight = (i + 1) % N;                // Cálculo para el palillo derecho
    }

    public void run() {                     // And away we go
        while (true) {
            think();                        // Llamada al metodo pensar
            take_forks();                   // Llamada al método para adquirir los palillos
            eat();                          // Llamada al método comer.
            grab();
            put_forks();                    // Llamada al método poner los palillos en la mesa
        }
    }

    public void take_forks() {               // Tomar los palillos que el filósofo necesita para comer.
        mutex.down();                       // Entrando en una región critica
        state[myNumber] = HUNGRY;           // Registrar el estado "hambriento"
        test(myNumber);                     // Intentar adquirir dos palillos para comer
        mutex.up();                         // saliendo de la región critica
        s[myNumber].down();                 // Entrar en estado de bloqueo si no se adqirieron palillos
    }

    public void put_forks() {
        mutex.down();                       // Entrando en región critica
        state[myNumber] = THINKING;         // Estado del filósofo establecido a "Pensando"
        test(myLeft);                       // Probar si el vecino de la izquierda puede comer con el palillo
        test(myRight);                      // Probar si el vecino de la derecha puede utilizar el palillo
        mutex.up();                         // Salir de la región critica
    }

    public void test(int k) {                // Prueba del filosofo
        // from 0 to N-1
        int onLeft = (k + N - 1) % N;           // K's palillo izquierdo
        int onRight = (k + 1) % N;             // K's  palillo derecho
        if (state[k] == HUNGRY
                && state[onLeft] != EATING
                && state[onRight] != EATING) {
            // Grab those forks
            state[k] = EATING;
            s[k].up();
        }
    }

    public void think() {
        System.out.println("Filosofo " + myNumber + " esta pensando");
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
        }
    }

    public void eat() {
        System.out.println("Filosofo " + myNumber + " esta comiendo");
        try {
            sleep(5000);
        } catch (InterruptedException ex) {
        }
    }

    public void grab() {
        System.out.println("El filosofo: " + myNumber + " ha liberado el Palillo " + myLeft + " y palillo " + myRight + " ahora estan disponibles");
        try {
            sleep(5000);
        } catch (InterruptedException ex) {
        }
    }

    public static void main(String args[]) {

        Philosopher p[] = new Philosopher[N];

        for (int i = 0; i < N; i++) {
            // Create each philosopher and their semaphore
            p[i] = new Philosopher(i);
            s[i] = new Semaphore(0);

            // Start the threads running
            p[i].start();
        }
    }
}
